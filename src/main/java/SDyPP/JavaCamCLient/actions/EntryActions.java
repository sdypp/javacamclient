package SDyPP.JavaCamCLient.actions;

import java.nio.file.Path;
import java.util.Date;

import SDyPP.JavaCamCLient.managers.FilesProccesManager;

public class EntryActions implements IEntryActions {
	
	@Override
	public void createAction(Path path) {
		System.out.println(new Date().toString() + ": Action Create " + path);
		FilesProccesManager.RegisterFile(path.normalize().toString());
	}
	
	@Override
	public void modifyAction(Path path) {
		//System.out.println(new Date().toString() + ": Action Modify " + path);
		FilesProccesManager.RegisterFile(path.normalize().toString());
		//FilesProccesManager.CheckFilesLoop();
	}
	
	@Override
	public void deleteAction(Path path) {
		System.out.println(new Date().toString() + ": Action Delete " + path);
	}

}
