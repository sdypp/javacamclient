package SDyPP.JavaCamCLient.actions;

import java.nio.file.Path;

public interface IEntryActions {

	void createAction(Path path);

	void modifyAction(Path path);

	void deleteAction(Path path);

}