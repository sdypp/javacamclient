package SDyPP.JavaCamCLient.managers;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


public class ApiConectionManager {

    public static String SimplePostRequest(String url, String filePath)
    {
    	HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        
        String responseBody = "";
        
        try {
            FileBody bin = new FileBody(new File(filePath));
            StringBody id = new StringBody("3");
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("Image", bin);

            httppost.setEntity(reqEntity);
            System.out.println("Requesting : " + httppost.getRequestLine());
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpclient.execute(httppost, responseHandler);
            System.out.println("responseBody : " + responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
        	return null;
        } finally {
            httpclient.getConnectionManager().shutdown();
            return responseBody;
        }
    }
    
    public static String SimplePostRequestJson(String url, String body)
    {
    	HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        
        String responseBody = "";
        
        try {
            String json = body;
            StringEntity entity = new StringEntity(json);
            httppost.setEntity(entity);
            httppost.setHeader("Accept", "application/json");
            httppost.setHeader("Content-type", "application/json");
            
            //httppost.setEntity(reqEntity);
            System.out.println("Requesting : " + httppost.getRequestLine());
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpclient.execute(httppost, responseHandler);
            System.out.println("responseBody : " + responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
        	return null;
        } finally {
            httpclient.getConnectionManager().shutdown();
            return responseBody;
        }
    }
}
