package SDyPP.JavaCamCLient.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

import SDyPP.JavaCamCLient.actions.EntryActions;

public class ApplicationManager {
	
	protected DirectoryWatcher watcher;
	public static Properties properties;
	private static Thread watcherThread;
	
	public void mainApplication() throws Exception {
		String propertiesFile = ApplicationManager.class.getResource("config.properties").getPath();
		 try (InputStream input = new FileInputStream(propertiesFile)) {
	            properties = new Properties();
	            // load a properties file
	            properties.load(input);

	        } catch (IOException ex) {
	            ex.printStackTrace();
	            throw new Exception("Missing Config file");
	        }
		
		launchProccesManager();
		launchDirectoryWatcher();
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("----------------------------------------------");
		System.out.println();
		System.out.println("Main Application Running");
		System.out.println("Press to end applications");
		System.out.println();
		System.out.println("----------------------------------------------");
		String s = in.nextLine();
		stopAll();
	}
	
	private void launchDirectoryWatcher() {
		System.out.println("Launching DirectoryWatcher");
		DirectoryWatcher.StartProccesLoop(new EntryActions());
	}
	
	private void launchProccesManager()
	{
		System.out.println("Launching ProccesManager");
		FilesProccesManager.ProccesAllFiles();
		FilesProccesManager.StartProccesLoop();
	}
	
	private void stopAll()
	{
		FilesProccesManager.StopProccesLoop();
		DirectoryWatcher.StopProccesLoop();
	}

}
