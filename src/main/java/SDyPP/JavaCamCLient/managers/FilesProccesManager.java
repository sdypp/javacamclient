package SDyPP.JavaCamCLient.managers;

import java.awt.List;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.nio.file.LinkOption;

import org.json.JSONObject;

public class FilesProccesManager extends Thread {
	
	private static HashMap<String, Long> FilesHash = new HashMap<String, Long>();
	private static List CurrentFiles = new List();
	private static List FilesReady = new List();
	
	private static long LAST_MOD_THRESHHOLD;
	
	private static Thread loop;
	
	public static void RegisterFile(String path)
	{
		LAST_MOD_THRESHHOLD = Long.parseLong(ApplicationManager.properties.getProperty("FILE_MOD_THRESHOLD"));
		long fileTime = GetFileTime(path); 
		if(fileTime == 0)
		{
			StoreFile(path);
		}else {
			UpdateFileTime(path);		
		}
	}
	
	public static void UpdateFileTime(String path)
	{
		FilesHash.replace(path,  System.nanoTime());
	}
	
	public static long GetFileTime(String path)
	{
		try {
			long value = FilesHash.get(path);
			return value;
		}catch(Exception e){
			return 0;
		}
	}
	
	public static void StoreFile(String path)
	{
		String file = path.toString();
		long time = System.nanoTime();
		FilesHash.put(file, time);
		CurrentFiles.add(path);
	}
	
	public static void CheckFilesLoop()
	{
		while(true)
		{		
			System.out.println("Procces Loop Running");
			for (String file : CurrentFiles.getItems()) {
				long time = GetFileTime(file);
				long duration = ((System.nanoTime() - time) / 100000);
				//System.out.println(file + " " + duration);
				if(duration >= LAST_MOD_THRESHHOLD)
				{
					System.out.println(file + " Ready for upload");
					FilesReady.add(file);
					CurrentFiles.remove(file);
				}
			}
			
			for (String file : FilesReady.getItems()) {
				File f = null;
				try {
					String path = ApplicationManager.properties.getProperty("FILE_DIRECTORY")+ "/"+ file;
					f = new File(path);
					String url = ApplicationManager.properties.getProperty("CAM_SERVICE_URL")+
							ApplicationManager.properties.getProperty("UPLOAD_PATH")+
							ApplicationManager.properties.getProperty("CAM_ID");
					String response = ApiConectionManager.SimplePostRequest(url, path);
					if(response.contains(file))
					{
						FilesReady.remove(file);
						FilesHash.remove(file);
						System.out.println("Uploaded: " + file);
					}
				} catch (Exception e) {
					// TODO: handle exception
					continue;
				}
				
				try {
					String url = ApplicationManager.properties.getProperty("MAIN_SERVICE_URL")+
							ApplicationManager.properties.getProperty("STORE_DATA_PATH");
					
					String body = new JSONObject()
							.put("cam_id", ApplicationManager.properties.getProperty("CAM_ID"))  
							.put("created_at", ((FileTime) Files.getAttribute(Paths.get(f.getAbsolutePath()), "creationTime", LinkOption.NOFOLLOW_LINKS )).toString())
			                .put("file_path", file).toString();
					
					String response = ApiConectionManager.SimplePostRequestJson(url, body);
					
					f.delete();
					break;
				} catch (Exception e) {
					// TODO: handle exception
					continue;
				}
				
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void ProccesAllFiles() {
		try (Stream<Path> walk = Files.walk(Paths.get(ApplicationManager.properties.getProperty("FILE_DIRECTORY")))) {

			java.util.List<String> result = walk.filter(Files::isRegularFile)
					.map(x -> x.getFileName().toString()).collect(Collectors.toList());

			result.forEach(x -> RegisterFile(x));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void StartProccesLoop() {
		StopProccesLoop();
		loop = new Thread(new FilesProccesManager());
		loop.start();
		
	}
	
	public static void StopProccesLoop()
	{
		if(loop != null) {
			loop.stop();
			loop.interrupt();
		}
		loop = null;
		System.out.println("Stoped File Procces Manager");
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		CheckFilesLoop();
	}

}
