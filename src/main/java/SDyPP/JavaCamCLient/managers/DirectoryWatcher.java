package SDyPP.JavaCamCLient.managers;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import SDyPP.JavaCamCLient.actions.EntryActions;
import SDyPP.JavaCamCLient.actions.IEntryActions;

import java.nio.file.WatchEvent.Kind;

public class DirectoryWatcher extends Thread {
	
	IEntryActions entryActions = null;
	private static Path directoryPath;
	private static Thread loop;
	
	public DirectoryWatcher(IEntryActions entryActions) {
		directoryPath = new File(ApplicationManager.properties.getProperty("FILE_DIRECTORY")).toPath();
		this.entryActions = entryActions;
	}
	
	@SuppressWarnings("unchecked")
	public void watchDirectoryPath() {
        // Sanity check - Check if path is a folder
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(directoryPath,
                    "basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
                throw new IllegalArgumentException("Path: " + directoryPath
                        + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }

        System.out.println("Watching path: " + directoryPath);

        // We obtain the file system of the Path
        FileSystem fs = directoryPath.getFileSystem();

        // We create the new WatchService using the new try() block
        try (WatchService service = fs.newWatchService()) {

            // We register the path to the service
            // We watch for creation events
        	directoryPath.register(service, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE); 

            // Start the infinite polling loop
            WatchKey key = null;
            while (true) {
                key = service.take();

                // Dequeueing events
                Kind<?> kind = null;
                for (WatchEvent<?> watchEvent : key.pollEvents()) {
                    // Get the type of the event
                    kind = watchEvent.kind();
                    if (OVERFLOW == kind) {
                        continue; // loop
                    } else if (ENTRY_CREATE == kind) {
                        Path newPath = ((WatchEvent<Path>) watchEvent).context();
                        
                    	this.entryActions.createAction(newPath);
                        //System.out.println("New path created: " + newPath);
                    } else if (ENTRY_MODIFY == kind) {
                        Path newPath = ((WatchEvent<Path>) watchEvent).context();
                        
                    	this.entryActions.modifyAction(newPath);
                        //System.out.println("New path modified: " + newPath);
                    }else if (ENTRY_DELETE == kind){
                        Path newPath = ((WatchEvent<Path>) watchEvent).context();
                        
                    	this.entryActions.deleteAction(newPath);
                    	//System.out.println("New path modified: " + newPath + " || " + kind);
                    }
                }

                if (!key.reset()) {
                    break; // loop
                }
                Thread.sleep(500);
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

    }
	
	public static void StartProccesLoop(IEntryActions entryActions) {
		StopProccesLoop();
		loop = new Thread(new DirectoryWatcher(entryActions));
		loop.start();
		
	}
	
	public static void StopProccesLoop()
	{
		if(loop != null) {
			loop.stop();
			loop.interrupt();
		}
		loop = null;
		System.out.println("Stoped Directory Watcher");
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		watchDirectoryPath();
	}
	
}
